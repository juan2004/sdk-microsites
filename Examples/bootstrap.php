<?php

/**
 * The only purpose for this file its bootstrap the classes and generate a single point
 * to instanciate the Microsite class
 */
require_once __DIR__ . '/../vendor/autoload.php';

use Placeto\MicrositesSdk\Microsite;

/*
 * Instanciates the Microsite object providing the login and tranKey, also the url that will be
 * used for the service
 */
function microsite(): Microsite
{
    return new Microsite([
        'login' => getenv('MICROSITE_LOGIN'),
        'secretKey' => getenv('MICROSITE_SECRETKEY'),
        'baseUrl' => getenv('MICROSITE_URL'),
    ]);
}