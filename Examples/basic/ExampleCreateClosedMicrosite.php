<?php

require_once __DIR__.'/../bootstrap.php';

class ExampleCreateClosedMicrosite
{
    public function createMicrositeClosed()
    {
        $request = [
            'name' => 'string',
            'alias' => 'example_string',
            'type' => 'CLOSED',
            'sites' => [5, 32],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
        ];

        $response = microsite()->createMicrosite($request);

        if ($response->isSuccessful()) {
            return $response->geturlResponse();
        } else {
            return $response->response;
        }
    }
}
