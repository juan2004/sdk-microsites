# Microsites Sdk

Software development kit to connect with the Microsites api

## Installation
You should add PlacetoPay repository:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://dev.placetopay.com/repository"
        }
    ]
}
```

Then, you can install the package via composer:
```
composer require placetopay/microsites-sdk
```

## Usage