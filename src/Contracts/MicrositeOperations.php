<?php

namespace Placeto\MicrositesSdk\Contracts;

use Placeto\MicrositesSdk\Helpers\Settings;
use Placeto\MicrositesSdk\Requests\CreateMicrositeRequest;
use Placeto\MicrositesSdk\Responses\CreateMicrositeResponse;

abstract class MicrositeOperations
{
    protected Settings $settings;

    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    abstract public function createMicrosite(CreateMicrositeRequest $createMicrositeRequest): CreateMicrositeResponse;
}
