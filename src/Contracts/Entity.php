<?php

namespace Placeto\MicrositesSdk\Contracts;

use Placeto\MicrositesSdk\Helpers\ArrayHelper;
use Placeto\MicrositesSdk\Traits\LoaderTrait;

abstract class Entity
{
    use LoaderTrait;

    abstract public function toArray(): array;

    protected function arrayFilter(array $array): array
    {
        return ArrayHelper::filter($array);
    }
}
