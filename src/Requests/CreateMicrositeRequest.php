<?php

namespace Placeto\MicrositesSdk\Requests;

use Placeto\MicrositesSdk\Contracts\Entity;
use Placeto\MicrositesSdk\Exceptions\MicrositeException;

class CreateMicrositeRequest extends Entity
{
    private const VERSION = 'v2';
    private const PAYMENT_EXPIRATION = 10;

    protected string $name;
    protected string $alias;
    protected string $type;
    protected array $categories;
    protected array $sites;
    protected int $loginFields;
    protected bool $allowPartial;
    protected string $version;
    protected int $paymentExpiration;

    public function __construct($data = [])
    {
        $this->load($data, ['name', 'alias', 'type', 'categories', 'sites', 'loginFields', 'allowPartial', 'version',
            'paymentExpiration', ]);
    }

    public function getName(): string
    {
        if (isset($this->name) && is_string($this->name) && strlen($this->name) >= 1) {
            return $this->name;
        } else {
            throw MicrositeException::forDataNotProvided('The field name is mandatory and must be of type string greater than or equal to one character');
        }
    }

    public function getAlias(): string
    {
        if (isset($this->alias) && is_string($this->alias) && strlen($this->alias) >= 1) {
            return $this->alias;
        } else {
            throw MicrositeException::forDataNotProvided('The field alias is mandatory and must be of type string greater than or equal to one character');
        }
    }

    public function getType(): string
    {
        if (isset($this->type) && ($this->type === 'OPEN' || $this->type === 'CLOSED')) {
            return $this->type;
        } else {
            throw MicrositeException::forDataNotProvided('The field type is mandatory and must be of type OPEN or CLOSED');
        }
    }

    public function getCategories(): array
    {
        if (isset($this->categories) && is_array($this->categories)) {
            return $this->categories;
        } else {
            throw MicrositeException::forDataNotProvided('The field categories is mandatory and must be of type array string');
        }
    }

    public function getSites(): array
    {
        if (isset($this->sites) && is_array($this->sites)) {
            return $this->sites;
        } else {
            throw MicrositeException::forDataNotProvided('The field sites is mandatory and must be of type array integer');
        }
    }

    public function getLoginFields(): int
    {
        if (isset($this->loginFields) && is_int($this->loginFields)) {
            return $this->loginFields;
        } else {
            throw MicrositeException::forDataNotProvided('The field loginFields is mandatory and must be of type integer');
        }
    }

    public function getVersion(): string
    {
        if (isset($this->version) && is_string($this->version)) {
            return $this->version;
        } else {
            return self::VERSION;
        }
    }

    public function getPaymentExpiration(): int
    {
        if (isset($this->paymentExpiration) && is_int($this->paymentExpiration)) {
            return $this->paymentExpiration;
        } else {
            return self::PAYMENT_EXPIRATION;
        }
    }

    public function isAllowPartial(): bool
    {
        if (isset($this->allowPartial) && is_bool($this->allowPartial)) {
            return $this->allowPartial;
        } else {
            throw MicrositeException::forDataNotProvided('The field allowPartial is mandatory and must be of type boolean');
        }
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'name' => $this->getName(),
            'alias' => $this->getAlias(),
            'type' => $this->getType(),
            'sites' => $this->getSites(),
            'allowPartial' => $this->isAllowPartial(),
            'categories' => $this->getCategories(),
            'loginFields' => $this->getLoginFields(),
            'version' => $this->getVersion(),
            'paymentExpiration' => $this->getPaymentExpiration(),
        ]);
    }
}
