<?php

namespace Placeto\MicrositesSdk;

use Placeto\MicrositesSdk\Exceptions\MicrositeException;
use Placeto\MicrositesSdk\Helpers\Settings;
use Placeto\MicrositesSdk\Requests\CreateMicrositeRequest;
use Placeto\MicrositesSdk\Responses\CreateMicrositeResponse;

class Microsite
{
    protected Settings $settings;

    public const MESSAGE = 'Wrong class request';

    public function __construct(array $data)
    {
        $this->settings = new Settings($data);
    }

    public function createMicrosite($dataRequest): CreateMicrositeResponse
    {
        if (is_array($dataRequest)) {
            $createMicrositeRequest = new CreateMicrositeRequest($dataRequest);
        }

        if (! ($createMicrositeRequest instanceof CreateMicrositeRequest)) {
            throw MicrositeException::forDataNotProvided(self::MESSAGE);
        }

        return $this->settings->getMicrositeOperations()->createMicrosite($createMicrositeRequest);
    }
}
