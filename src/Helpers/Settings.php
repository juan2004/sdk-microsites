<?php

namespace Placeto\MicrositesSdk\Helpers;

use GuzzleHttp\Client;
use Placeto\MicrositesSdk\Carrier\Authentication;
use Placeto\MicrositesSdk\Carrier\Request;
use Placeto\MicrositesSdk\Contracts\Entity;
use Placeto\MicrositesSdk\Contracts\MicrositeOperations;
use Placeto\MicrositesSdk\Exceptions\MicrositeException;

class Settings extends Entity
{
    private const MESSAGE1 = 'No login or secretKey provided';
    private const MESSAGE2 = 'No service URL provided to use';

    protected int $timeout = 15;
    protected bool $verifySsl = true;

    public string $baseUrl = '';

    protected string $login;
    protected string $secretKey;
    protected array $headers = [];

    protected ?Client $client = null;

    protected ?MicrositeOperations $micrositeOperations = null;

    public function __construct(array $data)
    {
        if (! isset($data['login']) || ! isset($data['secretKey'])) {
            throw MicrositeException::forDataNotProvided(self::MESSAGE1);
        }

        if (! isset($data['baseUrl']) || ! filter_var($data['baseUrl'], FILTER_VALIDATE_URL)) {
            throw MicrositeException::forDataNotProvided(self::MESSAGE2);
        }

        if (substr($data['baseUrl'], -1) != '/') {
            $data['baseUrl'] .= '/';
        }

        $allowedKeys = [
            'baseUrl',
            'login',
            'secretKey',
            'headers',
        ];

        $this->load($data, $allowedKeys);
    }

    public function baseUrl(string $endpoint = ''): string
    {
        return $this->baseUrl.$endpoint;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getTimeout(): int
    {
        return $this->timeout;
    }

    public function isVerifySsl(): bool
    {
        return $this->verifySsl;
    }

    public function getClient(): Client
    {
        if (! $this->client) {
            $this->client = new Client([
                'timeout' => $this->getTimeout(),
                'connect_timeout' => $this->getTimeout(),
                'verify' => $this->isVerifySsl(),
            ]);
        }

        return $this->client;
    }

    public function toArray(): array
    {
        return [];
    }

    public function getMicrositeOperations(): MicrositeOperations
    {
        if ($this->micrositeOperations instanceof MicrositeOperations) {
            return $this->micrositeOperations;
        } else {
            $this->micrositeOperations = new Request($this);
        }

        return $this->micrositeOperations;
    }

    public function authentication(): Authentication
    {
        return new Authentication([
            'login' => $this->getLogin(),
            'secretKey' => $this->getSecretKey(),
        ]);
    }
}
