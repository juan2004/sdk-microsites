<?php

namespace Placeto\MicrositesSdk\Responses;

use Placeto\MicrositesSdk\Contracts\Entity;

class CreateMicrositeResponse extends Entity
{
    private const ST_OK = 'OK';
    private const ST_FAILED = 'FAILED';

    public array $response = [];

    private ?string $urlResponse = '';

    private string $status;

    public function __construct($data = [])
    {
        $this->response = $data;
        $this->status = $data['status']['status'];
    }

    public function geturlResponse(): ?string
    {
        if ($this->isSuccessful()) {
            $this->urlResponse = $this->response['data']['url'];
        } else {
            $this->urlResponse = null;
        }

        return $this->urlResponse;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function isSuccessful(): bool
    {
        return $this->getStatus() == self::ST_OK;
    }

    public function isFailed(): bool
    {
        return $this->getStatus() == self::ST_FAILED;
    }

    public function toArray(): array
    {
        return $this->arrayFilter([
            'status' => $this->getStatus(),
            'response' => $this->response,
        ]);
    }
}
