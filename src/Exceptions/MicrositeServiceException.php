<?php

namespace Placeto\MicrositesSdk\Exceptions;

use Throwable;

class MicrositeServiceException extends MicrositeException
{
    public const MESSAGE = 'Error handling operation';

    public static function fromServiceException(Throwable $e): self
    {
        return new self(self::MESSAGE, 100, $e);
    }
}
