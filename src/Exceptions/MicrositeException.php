<?php

namespace Placeto\MicrositesSdk\Exceptions;

use Exception;
use Throwable;

class MicrositeException extends Exception
{
    public static function readException(Throwable $e): string
    {
        return $e->getMessage().' ON '.$e->getFile().' LINE '.$e->getLine().' ['.get_class($e).']';
    }

    public static function forDataNotProvided(string $message = ''): self
    {
        return new self($message);
    }
}
