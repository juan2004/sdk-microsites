<?php

namespace Placeto\MicrositesSdk\Carrier;

use Placeto\MicrositesSdk\Exceptions\MicrositeException;

class Authentication
{
    private const MESSAGE = 'No login or tranKey provided on authentication';

    private string $login;
    private string $secretKey;

    private array $auth = [];
    private bool $overridden = false;
    private string $algorithm = 'sha256';

    /**
     * @throws MicrositeException
     */
    public function __construct(array $config)
    {
        if (! isset($config['login']) || ! isset($config['secretKey'])) {
            throw MicrositeException::forDataNotProvided(self::MESSAGE);
        }

        $this->login = $config['login'];
        $this->secretKey = $config['secretKey'];

        if (isset($config['auth'])) {
            $this->auth = $config['auth'];
            $this->overridden = true;
        }

        $this->algorithm = $config['algorithm'] ?? 'sha256';

        $this->generate();
    }

    public function getNonce($encoded = true)
    {
        if ($this->auth) {
            $nonce = $this->auth['nonce'];
        } else {
            if (function_exists('openssl_random_pseudo_bytes')) {
                $nonce = bin2hex(openssl_random_pseudo_bytes(16));
            } elseif (function_exists('random_bytes')) {
                $nonce = bin2hex(random_bytes(16));
            } else {
                $nonce = mt_rand();
            }
        }

        if ($encoded) {
            return base64_encode($nonce);
        }

        return $nonce;
    }

    public function getSeed(): string
    {
        if ($this->auth) {
            return $this->auth['seed'];
        }

        return date('c');
    }

    public function generate(): self
    {
        if (! $this->overridden) {
            $this->auth = [
                'seed' => $this->getSeed(),
                'nonce' => $this->getNonce(),
            ];
        }

        return $this;
    }

    public function asArray(): array
    {
        return [
            'login' => $this->getLogin(),
            'tranKey' => $this->trankey(),
            'nonce' => $this->getNonce(true),
            'seed' => $this->getSeed(),
        ];
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    public function tranKey(): string
    {
        return base64_encode(hash($this->algorithm, $this->getNonce(false).$this->getSeed().$this->getSecretKey(), true));
    }
}
