<?php

namespace Placeto\MicrositesSdk\Carrier;

use GuzzleHttp\Exception\BadResponseException;
use Placeto\MicrositesSdk\Contracts\MicrositeOperations;
use Placeto\MicrositesSdk\Exceptions\MicrositeServiceException;
use Placeto\MicrositesSdk\Requests\CreateMicrositeRequest;
use Placeto\MicrositesSdk\Responses\CreateMicrositeResponse;
use Throwable;

class Request extends MicrositeOperations
{
    public const ENDPOINT_CREATE_MICROSITE = 'api/microsites';

    private function makeRequest(string $url, array $arguments): array
    {
        try {
            $data = array_merge(['auth' => $this->settings->authentication()->asArray(), 'name' => $arguments['name'],
                'categories' => $arguments['categories'], 'type' => $arguments['type'], 'alias' => $arguments['alias'],
                'sites' => $arguments['sites'], 'allowPartial' => $arguments['allowPartial'],
                'loginFields' => $arguments['loginFields'], 'version' => $arguments['version'],
                'paymentExpiration' => $arguments['paymentExpiration'], ]);

            $response = $this->settings->getClient()->post($url, [
                'json' => $data,
                'headers' => $this->settings->getHeaders(),
            ]);

            $result = $response->getBody()->getContents();
        } catch (BadResponseException $e) {
            $result = $e->getResponse()->getBody()->getContents();
        } catch (Throwable $e) {
            throw MicrositeServiceException::fromServiceException($e);
        }

        return json_decode($result, true);
    }

    public function createMicrosite(CreateMicrositeRequest $createMicrositeRequest): CreateMicrositeResponse
    {
        $result = $this->makeRequest($this->settings->baseUrl(self::ENDPOINT_CREATE_MICROSITE), $createMicrositeRequest->toArray());

        return new CreateMicrositeResponse($result);
    }
}
