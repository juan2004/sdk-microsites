<?php

namespace Tests\Unit;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Placeto\MicrositesSdk\Exceptions\MicrositeException;
use Placeto\MicrositesSdk\Helpers\Settings;
use Placeto\MicrositesSdk\Microsite;

class SettingsTest extends TestCase
{
    public function testNoLoginProvided(): void
    {
        $this->expectException(MicrositeException::class);
        $this->expectExceptionMessage('No login or secretKey provided');

        $micrositio = new Microsite([
            'secretKey' => 'P2P123#',
            'baseUrl' => 'https://dev.placetopay.com/microsites',
        ]);
    }

    public function testNoSecretKeyProvided(): void
    {
        $this->expectException(MicrositeException::class);
        $this->expectExceptionMessage('No login or secretKey provided');

        $micrositio = new Microsite([
            'login' => 'user_placetopay',
            'baseUrl' => 'https://dev.placetopay.com/microsites',
        ]);
    }

    public function testNoBaseUrlProvided(): void
    {
        $this->expectException(MicrositeException::class);
        $this->expectExceptionMessage('No service URL provided to use');

        $microsite = new Microsite([
            'login' => 'user_placetopay',
            'secretKey' => 'P2P123#',
        ]);
    }

    public function testBaseUrlOptions(): void
    {
        $microsite1 = new Microsite([
            'login' => 'user_placetopay',
            'secretKey' => 'P2P123#',
            'baseUrl' => 'https://dev.placetopay.com/microsites',
        ]);

        $this->assertInstanceOf(Microsite::class, $microsite1);

        $microsite2 = new Microsite([
            'login' => 'user_placetopay',
            'secretKey' => 'P2P123#',
            'baseUrl' => 'https://dev.placetopay.com/microsites/',
        ]);

        $this->assertInstanceOf(Microsite::class, $microsite2);
    }

    public function testCreateAClient(): void
    {
        $client = (new Settings([
            'login' => 'user_placetopay',
            'secretKey' => 'P2P123#',
            'baseUrl' => 'https://dev.placetopay.com/microsites',
        ]))->getClient();

        $this->assertInstanceOf(Client::class, $client);
    }
}
