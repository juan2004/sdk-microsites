<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Placeto\MicrositesSdk\Carrier\Authentication;

class AuthenticationTest extends TestCase
{
    public function testGenerateAuth(): void
    {
        $data = new Authentication([
            'login' => 'user_placetopay',
            'secretKey' => 'P2P123#',
        ]);

        $auth = $data->asArray();

        $this->assertIsArray($auth);
        $this->assertEquals('user_placetopay', $auth['login']);
        $this->assertEquals($data->tranKey(), $auth['tranKey']);
        $this->assertEquals($data->getNonce(), $auth['nonce']);
        $this->assertEquals(date('c'), $auth['seed']);
    }
}
