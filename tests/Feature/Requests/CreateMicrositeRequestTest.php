<?php

namespace Tests\Feature\Requests;

use PHPUnit\Framework\TestCase;
use Placeto\MicrositesSdk\Exceptions\MicrositeException;
use Placeto\MicrositesSdk\Requests\CreateMicrositeRequest;

class CreateMicrositeRequestTest extends TestCase
{
    public function testCorrectlyAnalyzeTheCreationOfOpenMicrosite(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'OPEN',
            'sites' => [5, 32],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $request = new CreateMicrositeRequest($data);

        $this->assertEquals($data['name'], $request->getName());
        $this->assertEquals($data['alias'], $request->getAlias());
        $this->assertEquals($data['type'], $request->getType());
        $this->assertEquals($data['sites'], $request->getSites());
        $this->assertEquals($data['allowPartial'], $request->isAllowPartial());
        $this->assertEquals($data['categories'], $request->getCategories());
        $this->assertEquals($data['loginFields'], $request->getLoginFields());

        $this->assertEquals($data, $request->toArray());
    }

    public function testCorrectlyAnalyzeTheCreationOfClosedMicrosite(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'CLOSED',
            'sites' => [10],
            'allowPartial' => false,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $request = new CreateMicrositeRequest($data);

        $this->assertEquals($data['name'], $request->getName());
        $this->assertEquals($data['alias'], $request->getAlias());
        $this->assertEquals($data['type'], $request->getType());
        $this->assertEquals($data['sites'], $request->getSites());
        $this->assertEquals($data['allowPartial'], $request->isAllowPartial());
        $this->assertEquals($data['categories'], $request->getCategories());
        $this->assertEquals($data['loginFields'], $request->getLoginFields());

        $this->assertEquals($data, $request->toArray());
    }

    public function testTheNameFieldIsMandatory(): void
    {
        $data = [
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'CLOSED',
            'sites' => [10],
            'allowPartial' => false,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $this->assertArrayNotHasKey('name', $data);

        $this->expectException(MicrositeException::class);

        $this->expectExceptionMessage('The field name is mandatory and must be of type string greater than or equal to one character');

        $request = (new CreateMicrositeRequest($data))->toArray();
    }

    public function testTheAliasFieldIsMandatory(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'type' => 'CLOSED',
            'sites' => [10],
            'allowPartial' => false,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $this->assertArrayNotHasKey('alias', $data);

        $this->expectException(MicrositeException::class);

        $this->expectExceptionMessage('The field alias is mandatory and must be of type string greater than or equal to one character');

        $request = (new CreateMicrositeRequest($data))->toArray();
    }

    public function testTheTypeFieldIsMandatory(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'sites' => [10],
            'allowPartial' => false,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $this->assertArrayNotHasKey('type', $data);

        $this->expectException(MicrositeException::class);

        $this->expectExceptionMessage('The field type is mandatory and must be of type OPEN or CLOSED');

        $request = (new CreateMicrositeRequest($data))->toArray();
    }

    public function testTheSitesFieldIsMandatory(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'CLOSED',
            'allowPartial' => false,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $this->assertArrayNotHasKey('sites', $data);

        $this->expectException(MicrositeException::class);

        $this->expectExceptionMessage('The field sites is mandatory and must be of type array integer');

        $request = (new CreateMicrositeRequest($data))->toArray();
    }

    public function testTheAllowPartialFieldIsMandatory(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'CLOSED',
            'sites' => [10],
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $this->assertArrayNotHasKey('allowPartial', $data);

        $this->expectException(MicrositeException::class);

        $this->expectExceptionMessage('The field allowPartial is mandatory and must be of type boolean');

        $request = (new CreateMicrositeRequest($data))->toArray();
    }

    public function testTheCategoriesFieldIsMandatory(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'CLOSED',
            'sites' => [10],
            'allowPartial' => false,
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $this->assertArrayNotHasKey('categories', $data);

        $this->expectException(MicrositeException::class);

        $this->expectExceptionMessage('The field categories is mandatory and must be of type array string');

        $request = (new CreateMicrositeRequest($data))->toArray();
    }

    public function testTheLoginFieldsFieldIsMandatory(): void
    {
        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'CLOSED',
            'sites' => [10],
            'allowPartial' => false,
            'categories' => ['test_tecnologia'],
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $this->assertArrayNotHasKey('loginFields', $data);

        $this->expectException(MicrositeException::class);

        $this->expectExceptionMessage('The field loginFields is mandatory and must be of type integer');

        $request = (new CreateMicrositeRequest($data))->toArray();
    }
}
