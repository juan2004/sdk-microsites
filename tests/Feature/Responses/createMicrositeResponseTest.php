<?php

namespace Tests\Feature\Responses;

use PHPUnit\Framework\TestCase;
use Placeto\MicrositesSdk\Microsite;
use Placeto\MicrositesSdk\Responses\CreateMicrositeResponse;

class createMicrositeResponseTest extends TestCase
{
    public function testRedirectionResponseWithStatusSuccessful(): void
    {
        $Response = new createMicrositeResponse([
            'status' => [
                'status' => 'OK',
                'reason' => 201,
                'message' => 'Microsite created successfully',
                'date' => date('c'),
            ],
            'data' => [
            'id' => 457,
            'url' => 'https://dev.placetopay.com/microsites/7aeacb97ebfb2f3d86ea',
            ],
        ]);

        $this->assertTrue($Response->isSuccessful());
    }

    public function testRedirectionResponseWithStatusFailed(): void
    {
        $Response = new createMicrositeResponse([
            'status' => [
                'status' => 'FAILED',
                'reason' => 401,
                'message' => 'Authentication failed 102',
                'date' => '2022-10-30T22:03:54-05:00',
            ],
        ]);

        $this->assertTrue($Response->isFailed());
    }

    public function testRedirectMicrosite(): void
    {
        $microsite = new Microsite([
            'login' => 'user_placetopay',
            'secretKey' => 'P2P123#',
            'baseUrl' => 'https://dev.placetopay.com/microsites',
        ]);

        $data = [
            'name' => bin2hex(random_bytes(10)),
            'alias' => bin2hex(random_bytes(10)),
            'type' => 'OPEN',
            'sites' => [5, 32],
            'allowPartial' => true,
            'categories' => ['test_tecnologia'],
            'loginFields' => 1,
            'version' => 'v2',
            'paymentExpiration' => 10,
        ];

        $response = $microsite->createMicrosite($data);

        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('status', $response);

        if ($response->isSuccessful()) {
            $this->assertIsString($response->geturlResponse());
        } else {
            $this->assertIsArray($response->response);
        }
    }
}
